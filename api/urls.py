from django.conf.urls import url
from api import views

urlpatterns = [
    url(r'^IssueBook/$', views.IssueBook.as_view(), name='IssueBook'),
    url(r'^IssueChapter/$', views.IssueChapter.as_view(), name='IssueChapter'),
]
