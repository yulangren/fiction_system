#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Time: 2020/4/16  17:27
# @Author: 余浪人
# @email: yulangren520@gmail.com
from django.db import models
import uuid


class BaseModel(models.Model):
    uuid = models.UUIDField(verbose_name='uuid', default=uuid.uuid4(), unique=True,primary_key=True)
    is_delete = models.BooleanField(default=False, verbose_name='是否删除')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True,db_index=True, verbose_name='修改时间')

    class Meta:
        abstract = True

    def __getitem__(self, item):
        if hasattr(self, item):
            return getattr(self, item)
