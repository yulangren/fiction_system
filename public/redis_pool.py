#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Time: 2020/6/15  15:44
# @Author: 余浪人
# @email: yulangren520@gmail.com
import redis
from tl_book.settings import REDIS_HOST, REDIS_MAX_CONNECTIONS, REDIS_PASSWORD, REDIS_PORT

POOL = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PASSWORD, max_connections=REDIS_MAX_CONNECTIONS)
conn = redis.Redis(connection_pool=POOL) # 默认0号库
