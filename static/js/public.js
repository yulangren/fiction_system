// 消息通知
function notify(obj, title, msg, type) {
    obj.$notify({
        title: title,
        message: msg,
        type: type
    });
}

// form 提交
function submit_ajax(obj, method = 'POST', data_json = {}, url = '', refresh = true, is_show = true) {
    result = {};
    $.ajax({
        type: method,
        url: url,
        data: data_json,
        dataType: 'json',
        async: false,
        timeout:10000,
        headers: {'X-CSRFToken': this.getCookie('csrftoken')},
        success: function (response) {
            result = response;
            if (response.code === 0) {
                if (is_show) {
                    notify(obj, '操作成功', response.msg, 'success');
                }
                if (refresh) {
                    setTimeout(function () {
                        location.replace(location.href);
                    }, 2000);
                }
            } else if (response.code === 4) {
                if (is_show) {
                    notify(obj, response.msg, response.error, 'warning');
                }
            } else {
                if (is_show) {
                    notify(obj, response.msg, response.error, 'error');
                }
            }
        }, error: function (err) {
            notify(obj, '服务器处理异常', '请联系管理员处理！', 'warning');
        },
        complete: function (XMLHttpRequest, textStatus) {
            if(textStatus === 'timeout'){
                notify(obj, '请求超时', '请求超时或服务器处理时间过长', 'warning');
            }
        }

    });
    return result
}

// 重置form
function form_reset() {
    $('form')[0].reset();
}

// 获取Cookie
function getCookie(name) {
    var value = '; ' + document.cookie;
    var parts = value.split('; ' + name + '=');
    if (parts.length === 2) return parts.pop().split(';').shift()
}

// 删除单条数据
function Delete(obj, index, row) {
    obj.$confirm('此操作将永久删除该数据, 是否继续?', '操作提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
    }).then(() => {
        $.ajax({
            type: 'DELETE',
            data: {"uuid": row.uuid},
            dataType: 'json',
            timeout:10000,
            headers: {'X-CSRFToken': this.getCookie('csrftoken')},
            success: function (response) {
                if (response.code === 0) {
                    $('tr:eq(' + index + 1 + ')').remove();
                    notify(obj, '操作成功', response.msg, 'success');
                    setTimeout(function () {
                        location.replace(location.href);
                    }, 2000);
                } else if (response.code === 4) {
                    notify(obj, response.msg, response.error, 'warning');
                } else {
                    notify(obj, response.msg, response.error, 'error');
                }
            }, error: function (err) {
                notify(obj, '服务器处理异常', '请联系管理员处理！', 'warning');
            },complete: function (XMLHttpRequest, textStatus) {
            if(textStatus === 'timeout'){
                notify(obj, '请求超时', '请求超时或服务器处理时间过长', 'warning');
            }
        }
        })

    }).catch(() => {

    });


}

// 批量删除
function Deletes(obj, data) {
    obj.$confirm('此操作将永久删除数据, 是否继续?', '操作提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
    }).then(() => {
        $.ajax({
            type: 'POST',
            data: {"uuidList": data.join('|*TL*|')},
            dataType: 'json',
            timeout:20000,
            headers: {'X-CSRFToken': this.getCookie('csrftoken')},
            success: function (response) {
                if (response.code === 0) {
                    notify(obj, '操作成功', response.msg, 'success');
                    setTimeout(function () {
                        location.replace(location.href);
                    }, 2000);
                } else if (response.code === 4) {
                    notify(obj, response.msg, response.error, 'warning');
                } else {
                    notify(obj, response.msg, response.error, 'error');
                }
            }, error: function (err) {
                notify(obj, '服务器处理异常', '请联系管理员处理！', 'warning');
            },complete: function (XMLHttpRequest, textStatus) {
            if(textStatus === 'timeout'){
                notify(obj, '请求超时', '请求超时或服务器处理时间过长', 'warning');
            }
        }
        })

    }).catch(() => {

    });
}

// 日期时间对象转字符串
function dataTime_to_string(val) {
    return val.getFullYear() + "-" + (val.getMonth() + 1) + "-" + val.getDate() + " " + val.getHours() + ":" + val.getMinutes() + ":" + val.getSeconds() + "." + val.getMilliseconds();
}


// 上传文件
function _UploadFile(obj, url) {
    var formFile = new FormData();
    var fileObj = document.getElementById("UploadFile").files[0];
    formFile.append("file", fileObj);
    $.ajax({
        url: url,
        data: formFile,
        type: "POST",
        dataType: "json",
        timeout:60000,
        cache: false,//上传文件无需缓存
        processData: false,//用于对data参数进行序列化处理 这里必须false
        contentType: false, //必须
        headers: {'X-CSRFToken': this.getCookie('csrftoken')},
        success: function (result) {
            if (result.code === 0) {
                notify(obj, '上传成功', '上传文件成功！', 'success');
                return result
            } else {
                notify(obj, '上传失败', result.msg, 'error');
            }
        },complete: function (XMLHttpRequest, textStatus) {
            if(textStatus === 'timeout'){
                notify(obj, '请求超时', '请求超时或服务器处理时间过长', 'warning');
            }
        }
    })
}

// {% url 'admin_system:uploadFile' %}
$(function () {
    $('#uploadBtn').on('click', function () {
        $('input[name="file"]').click();
    });// 激活上传文件弹窗
    $('button[class="el-button el-button--default is-plain"]').on('click', function () {
        location.replace(location.href);
    }); // 刷新按钮

});