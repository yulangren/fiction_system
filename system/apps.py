from django.apps import AppConfig


class AdminSystemConfig(AppConfig):
    name = 'system'
