#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Time: 2020/4/14  15:17
# @Author: 余浪人
# @email: yulangren520@gmail.com
from django import forms
from django.forms import widgets


class SystemForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs = {'clearable': 'clearable'}

    web_name = forms.CharField(128, 1, label='站点名称', error_messages={'required': '请输入站点名称', 'max_length': '站点名称超出最长限制'}, required=True)
    web_subtitle = forms.CharField(128, label='站点副标题', error_messages={'required': '请输入站点副标题', 'max_length': '副标题超出最长限制'}, required=False)
    web_domain = forms.URLField(128, 1, label='站点域名', error_messages={'required': '请输入域名', 'max_length': '域名超出最长限制'}, required=True)
    web_logo = forms.CharField(512, label='站点LOGO', error_messages={'required': '请输入站点LOGO', 'max_length': 'LOGO超出最长限制'}, required=False)
    web_key = forms.CharField(512, label='站点关键词', error_messages={'required': '请输入站点关键词', 'max_length': '关键词超出最长限制'}, required=False, widget=widgets.Textarea(
        attrs={'placeholder': '请输入站点关键词'}
    ))
    web_description = forms.CharField(512, label='站点简介', error_messages={'max_length': '超出最长限制'}, required=False, widget=widgets.Textarea(
        attrs={'placeholder': '请输入站点简介'}
    ))
    web_stat = forms.CharField(2048, label='第三方统计', error_messages={'required': '请输入第三方统计代码', 'max_length': '超出最长限制'}, required=False, widget=widgets.Textarea(
        attrs={'placeholder': '请输入第三方统计'}
    ))
    web_close_tips = forms.CharField(2048, label='站点提示', error_messages={'required': '请输入关闭站点提示信息', 'max_length': '超出最长限制'}, required=False)
    web_info_copyright = forms.CharField(2048, label='版权显示', error_messages={'required': '请输入版权显示信息', 'max_length': '超出最长限制'}, required=False)
    web_record = forms.CharField(32, label='ICP备案', error_messages={'required': '请输入版权显示信息', 'max_length': '超出最长限制'}, required=False)
    web_gongan = forms.CharField(32, label='公安备案', error_messages={'required': '请输入版权显示信息', 'max_length': '超出最长限制'}, required=False)
    web_contact_qq = forms.CharField(64, label='联系QQ', error_messages={'required': '请输入版权显示信息', 'max_length': '超出最长限制'}, required=False)
    web_contact_email = forms.EmailField(128, label='联系邮箱', error_messages={'required': '请输入版权显示信息', 'max_length': '超出最长限制'}, required=False)
    web_on_off = forms.ChoiceField(choices=((1, '开启'), (2, '关闭')),  label='站点开关')
    web_content_on_off = forms.ChoiceField(choices=((1, '停用'), (2, '启用'),(3,'vip')),  label='评论开关')


class NavigationForm(forms.Form):
    column_name = forms.CharField(label='分类名称', max_length=16, error_messages={'required': '分类名称不可为空！', 'max_length': '分类名称过长'}, required=True)
    column_parent = forms.ChoiceField(label='上级分类', initial='顶级分类', required=True, error_messages={'required': '请正确选择顶级分类'},choices=[],widget=widgets.Select)
    column_order = forms.IntegerField(label='分类排序', max_value=999, min_value=0, error_messages={'max_value': '排序超过最大值999', 'min_value': '排序不可为负数'}, initial=0)
    column_link = forms.URLField(label='分类外链', max_length=128, error_messages={'max_length': '分类外链过长'}, required=False)
    column_keyword = forms.CharField(max_length=1024, error_messages={'max_length': '关键词过长'}, widget=widgets.Textarea(attrs={'placeholder': '请输入分类关键词'}), required=False, label='分类关键词')
    column_description = forms.CharField(max_length=1024, error_messages={'max_length': '描述过长'}, widget=widgets.Textarea(attrs={'placeholder': '请输入分类描述'}), required=False, label='分类描述')

    def __init__(self, *args, **kwargs):
        super(NavigationForm, self).__init__(*args, **kwargs)
        from .models import NavigationModel
        data_list = list(NavigationModel.objects.all().values_list('uuid', 'column_name'))
        data_list.insert(0,('0','顶级分类'))
        self.fields['column_parent'].choices = data_list

    def clean_column_name(self):
        from .models import NavigationModel
        column_name = self.cleaned_data.get('column_name')
        exists = NavigationModel.objects.filter(column_name=column_name).exists()
        if exists:
            raise forms.ValidationError("分类已经存在！")
        return column_name


class NavigationEditForm(forms.Form):
    column_name = forms.CharField(label='分类名称', max_length=16, error_messages={'required': '分类名称不可为空！', 'max_length': '分类名称过长'}, required=True)
    column_parent = forms.ChoiceField(label='上级分类', initial='顶级分类', required=True, error_messages={'required': '请正确选择顶级分类'},choices=[],widget=widgets.Select)
    column_order = forms.IntegerField(label='分类排序', max_value=999, min_value=0, error_messages={'max_value': '排序超过最大值999', 'min_value': '排序不可为负数'}, initial=0)
    column_link = forms.URLField(label='分类外链', max_length=128, error_messages={'max_length': '分类外链过长'}, required=False)
    column_keyword = forms.CharField(max_length=1024, error_messages={'max_length': '关键词过长'}, widget=widgets.Textarea(attrs={'placeholder': '请输入分类关键词'}), required=False, label='分类关键词')
    column_description = forms.CharField(max_length=1024, error_messages={'max_length': '描述过长'}, widget=widgets.Textarea(attrs={'placeholder': '请输入分类描述'}), required=False, label='分类描述')

    def __init__(self, *args, **kwargs):
        super(NavigationEditForm, self).__init__(*args, **kwargs)
        from .models import NavigationModel
        data_list = list(NavigationModel.objects.all().values_list('uuid', 'column_name'))
        data_list.insert(0,('0','顶级分类'))
        self.fields['column_parent'].choices = data_list


class LinkForm(forms.Form):
    link_name = forms.CharField(max_length=255, error_messages={'required': '链接名称不可为空！', 'max_length': '链接名称过长'}, required=True)
    link_url = forms.URLField(128, 1, error_messages={'required': '请输入链接地址', 'max_length': '链接超出最长限制'}, required=True)
    link_end_date= forms.DateTimeField(required=True,error_messages={'required':'请填写到期时间'})
    link_sort = forms.IntegerField(max_value=999,min_value=0,required=True,error_messages={'required':'请填写排序权重','min_value':'不可为负数'})
    link_remark= forms.CharField(max_length=255,required=False, error_messages={'max_length': '备注信息过长'})

    def clean_link_sort(self):
        link_sort = self.cleaned_data.get('link_sort')
        try:
            int(link_sort)
        except:
            raise forms.ValidationError("链接排序只能为数字！")
        return link_sort


class AdForm(forms.Form):
    ad_name = forms.CharField(max_length=128, error_messages={'required': '广告名称不可为空！', 'max_length': '广告名称过长'}, required=True)
    ad_code = forms.CharField(max_length=5120, error_messages={'required': '广告内容不可为空！', 'max_length': '广告内容过长'}, required=True)
    ad_end_date = forms.DateTimeField(required=True, error_messages={'required': '请填写到期时间'})
    ad_remark = forms.CharField(max_length=255, required=False, error_messages={'max_length': '备注信息过长'})


class SaveForm(forms.Form):
    save_classification = forms.IntegerField(2,1,required=True,error_messages={'required':'请选择存储类型','max_value':'存储类型错误','min_value':'存储类型错误'})
    save_path = forms.CharField(max_length=128, error_messages={'required': '存储路径不可为空！', 'max_length': '存储路径过长'}, required=True)
    bucket_name = forms.CharField(max_length=64, error_messages={'max_length': '空间名称 过长'}, required=False)
    bucket_domain = forms.URLField(max_length=64, error_messages={'max_length': '空间域名 过长'}, required=False)
    access_key = forms.CharField(max_length=64, error_messages={'max_length': 'ACCESS_KEY 过长'}, required=False)
    secret_key = forms.CharField(max_length=64, error_messages={'max_length': 'SECRET_KEY 过长'}, required=False)

    def clean_save_classification(self):
        save_classification =self.cleaned_data.get('save_classification')
        if save_classification==2:
            if not self.cleaned_data.get('bucket_name'):raise forms.ValidationError("空间名称 不可为空！")
            if not self.cleaned_data.get('bucket_domain'):raise forms.ValidationError("空间域名 不可为空！")
            if not self.cleaned_data.get('access_key'):raise forms.ValidationError("ACCESS_KEY 不可为空！")
            if not self.cleaned_data.get('secret_key'):raise forms.ValidationError("SECRET_KEY 不可为空！")
        return save_classification