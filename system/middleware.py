#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Time: 2020/8/1  9:58
# @Author: 余浪人
# @email: yulangren520@gmail.com
import time, uuid
from django.http.response import HttpResponse
from django.utils.deprecation import MiddlewareMixin


class MyMiddleware(MiddlewareMixin):
    __error = None

    def __init__(self, get_response=None):
        super(MyMiddleware, self).__init__(get_response)

    def process_request(self, request):
        pass

    def process_response(self, request, response):
        if self.__error:
            return HttpResponse(self.__error)
        return response

    def process_exception(self, request, exception):
        self.__error = str(exception)
