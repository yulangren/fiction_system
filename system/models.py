from db import BaseModel, models


# Create your models here.

class SystemModel(BaseModel):
    web_name = models.CharField(max_length=128, blank=True, verbose_name='网站名称')
    web_subtitle = models.CharField(max_length=128, blank=True, verbose_name='网站副标题')
    web_domain = models.URLField(max_length=128, blank=True, verbose_name='站点域名')
    web_logo = models.CharField(max_length=128, blank=True, verbose_name='站点LOGO')
    web_key = models.CharField(max_length=512, blank=True, verbose_name='站点关键词')
    web_description = models.CharField(max_length=512, blank=True, verbose_name='站点简介')
    web_stat = models.CharField(max_length=512, blank=True, verbose_name='第三方统计')
    web_close_tips = models.CharField(max_length=512, blank=True, verbose_name='站点提示')
    web_info_copyright = models.CharField(max_length=2048, blank=True, verbose_name='版权显示')
    web_record = models.CharField(max_length=32, blank=True, verbose_name='ICP备案')
    web_gongan = models.CharField(max_length=32, blank=True, verbose_name='公安备案')
    web_contact_qq = models.CharField(max_length=64, blank=True, verbose_name='联系QQ')
    web_contact_email = models.CharField(max_length=128, blank=True, verbose_name='联系邮箱')
    web_on_off = models.IntegerField(default=1, verbose_name='站点开关')
    web_content_on_off = models.IntegerField(default=2, verbose_name='系统评论| 1:停用  2:启用 3:仅vip')

    def __str__(self):
        return f'网站名称: {self.web_name}'

    class Meta:
        verbose_name_plural = '系统配置'
        verbose_name = '系统配置'

    def keys(self):
        return 'web_name', 'web_subtitle', 'web_domain', 'web_logo', 'web_key', 'web_description', 'web_stat', 'web_close_tips', 'web_info_copyright', 'web_record', 'web_gongan', 'web_contact_qq', 'web_contact_email', 'web_on_off', 'web_content_on_off', 'uuid'


class NavigationModel(BaseModel):
    column_name = models.CharField(max_length=16, verbose_name='分类名称')
    column_parent = models.CharField(max_length=64, verbose_name='上级分类', db_index=True)
    column_order = models.IntegerField(default=0, verbose_name='分类排序')
    column_keyword = models.CharField(max_length=1024, blank=True, verbose_name='分类关键词')
    column_description = models.CharField(max_length=1024, blank=True, verbose_name='分类描述')
    column_link = models.CharField(max_length=1024, blank=True, verbose_name='外链')
    column_index_show = models.BooleanField(default=True, verbose_name='首页显示', blank=True)
    column_display = models.BooleanField(default=True, verbose_name='导航显示', blank=True)

    def __str__(self):
        return f'分类名称： {self.column_name}'

    class Meta:
        verbose_name_plural = '小说分类'
        verbose_name = '小说分类'

    def keys(self):
        return 'column_name', 'column_parent', 'column_order', 'column_keyword', 'column_description', 'column_link', 'update_time', 'uuid'#,'column_index_show','column_display'


class NovelModel(BaseModel):
    novel_name = models.CharField(max_length=64, verbose_name='小说名称', db_index=True)
    categories = models.ForeignKey(NavigationModel, on_delete=models.CASCADE, related_name='novel_categories', verbose_name='所属分类')
    novel_author = models.CharField(max_length=64, blank=True, verbose_name='作者')
    novel_state = models.BooleanField(default=True, verbose_name='状态，是 完结|否 连载')
    novel_click = models.IntegerField(default=0, verbose_name='点击数')
    novel_img = models.CharField(max_length=1024, verbose_name='封面图', blank=True)
    novel_zone = models.IntegerField(default=0, verbose_name='专区分类、1男生|2女生|0全部')
    integral = models.IntegerField(default=0, verbose_name='积分数')
    novel_key = models.CharField(max_length=512, blank=True, verbose_name='关键词')
    novel_description = models.CharField(max_length=2048, blank=True, verbose_name='简介')
    novel_praise = models.IntegerField(default=0, verbose_name='点赞数')
    novel_love = models.IntegerField(default=0, verbose_name='喜欢数')
    novel_words = models.CharField(default='', max_length=30, blank=True, verbose_name='字数')
    novel_is_focus = models.BooleanField(default=False, verbose_name='截点图')
    novel_is_recommend = models.BooleanField(default=False, verbose_name='推荐')

    class Meta:
        verbose_name_plural = '小说'
        verbose_name = '小说'

    def __str__(self):
        return f'当前小说：<{self.novel_name}>'

    def keys(self):
        return 'novel_name', 'categories', 'novel_author', 'novel_author', 'novel_state', 'novel_click', 'novel_img', 'novel_zone', 'integral', 'update_time', 'novel_key', 'novel_description', 'novel_praise', 'novel_love', 'novel_words', 'uuid'#,'novel_is_focus','novel_is_recommend'


class ChapterModel(BaseModel):
    novel = models.ForeignKey(NovelModel, on_delete=models.CASCADE, related_name='chapter_novel', verbose_name='小说书名')
    title = models.CharField(max_length=128, verbose_name='章节名称')
    is_pay = models.BooleanField(default=False, verbose_name='是否付费', blank=True)
    content_id = models.CharField(max_length=8, default='', verbose_name='内容ID')
    content_path = models.CharField(max_length=32, default='', verbose_name='内容目录')

    class Meta:
        verbose_name_plural = '章节'
        verbose_name = '章节'

    def __str__(self):
        return f'章节：<{self.title}>'

    def keys(self):
        return 'title', 'is_pay', 'novel', 'update_time', 'content_id', 'content_path'


class Notice_Model(BaseModel):
    notice_title = models.CharField(max_length=128, verbose_name='公告标题')
    notice_promulgator = models.CharField(max_length=16, verbose_name='公告发布者')
    notice_content = models.TextField(verbose_name='公告内容')

    class Meta:
        verbose_name_plural = '公告'
        verbose_name = '公告'

    def __str__(self):
        return f'当前公告:<{self.notice_title}>'

    def keys(self):
        return 'notice_title', 'notice_promulgator', 'notice_content'


class Ad_Model(BaseModel):
    ad_name = models.CharField(max_length=128, verbose_name='广告名称')
    ad_code = models.TextField(verbose_name='广告代码')
    ad_end_date = models.DateTimeField(max_length=5120, verbose_name='到期时间')
    ad_remark = models.CharField(max_length=255, default='', blank=True, verbose_name='广告备注')

    class Meta:
        verbose_name_plural = '广告'
        verbose_name = '广告'

    def __str__(self):
        return f'广告名称:{self.ad_name}'

    def keys(self):
        return 'ad_name', 'ad_code', 'ad_end_date', 'ad_remark', 'uuid',


class Blogroll_Model(BaseModel):
    link_name = models.CharField(max_length=255, verbose_name='链接名称')
    link_url = models.URLField(verbose_name='链接地址')
    link_end_date = models.DateTimeField(verbose_name='到期时间')
    link_sort = models.IntegerField(default=0, verbose_name='链接排序')
    link_remark = models.CharField(max_length=255, blank=True, verbose_name='备注说明')

    class Meta:
        verbose_name_plural = '友情链接'
        verbose_name = '友情链接'

    def __str__(self):
        return f'链接名称: {self.link_name}'

    def keys(self):
        return 'link_name', 'link_url', 'link_end_date', 'link_sort', 'link_remark', 'uuid'


class Save_Model(BaseModel):
    save_classification = models.IntegerField(default=1, verbose_name='存储方式')
    save_path = models.CharField(max_length=128, default='/uploads', verbose_name='存储目录')
    bucket_name = models.CharField(max_length=64, blank=True, verbose_name='空间名称')
    bucket_domain = models.CharField(max_length=64, blank=True, verbose_name='空间域名')
    access_key = models.CharField(max_length=64, blank=True, verbose_name='ACCESS_KEY')
    secret_key = models.CharField(max_length=64, blank=True, verbose_name='SECRET_KEY')

    class Meta:
        verbose_name_plural = '存储方式'
        verbose_name = '存储方式'

    def __str__(self):
        return f'七牛云空间: {self.bucket_name}'

    def keys(self):
        return 'save_classification', 'save_path', 'bucket_name', 'access_key', 'secret_key', 'bucket_domain', 'uuid'


class Comment_Model(BaseModel):
    content_uuid = models.ForeignKey(NovelModel, on_delete=models.CASCADE, related_name='comment_model', verbose_name='小说ID')
    comment_txt = models.TextField(verbose_name='评论内容')
    user_qq = models.CharField(max_length=12, blank=True, verbose_name='用户QQ')
    user_name = models.CharField(max_length=32, blank=True, verbose_name='用户昵称')

    class Meta:
        verbose_name_plural = '评论'
        verbose_name = '评论'

    def __str__(self):
        return f'评论用户: <{self.user_name}>'

    def keys(self):
        return 'content_uuid', 'comment_txt', 'user_qq', 'user_name', 'update_time', 'status'


class Tags_Model(BaseModel):
    tag_name = models.CharField(max_length=255, unique=True, verbose_name='标签名')
    tag_click = models.IntegerField(default=0, verbose_name='访问数')

    class Meta:
        verbose_name_plural = '小说标签'
        verbose_name = '小说标签'

    def __str__(self):
        return f'标签名: <{self.tag_name}>'

    def keys(self):
        return 'tag_name', 'tag_click'
