#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Time: 2020/4/18  11:25
# @Author: 余浪人
# @email: yulangren520@gmail.com
import time

from django.template import Library

register = Library()


@register.filter
def column_parent(column_uuid):
    if column_uuid == '0' and len(column_uuid) < 36: return '顶级分类'
    from ..models import NavigationModel
    obj = NavigationModel.objects.filter(uuid=column_uuid).first()
    if obj: return obj.column_name
    return '识别失败'


@register.filter
def is_end(value: bool):
    if value: return '完结'
    return '连载中'


@register.filter
def time_str(value: float):
    try:
        res = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(value))
    except Exception as e:
        return f'时间解析失败：{str(e)}'
    return res
