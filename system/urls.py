from django.conf.urls import url
from system import views
from system import api_views

urlpatterns = [
    url(r'^systemPage/$', views.SystemPage.as_view(), name='system'),
    url(r'^navigationList/$', views.NavigationList.as_view(), name='navigation'),
    url(r'^NavigationAdd/$', views.Navigation.as_view(), name='navigationAdd'),
    url(r'^NavigationEdit/$', views.NavigationEdit.as_view(), name='navigationEdit'),
    url(r'^novelList/$', views.NovelList.as_view(), name='bookList'),
    url(r'^chapterGet/$', api_views.ChapterGet.as_view(), name='chapterGet'),
    url(r'^commentList/$', views.CommentView.as_view(), name='comment'),
    url(r'^linkLink/$', views.LinkView.as_view(), name='linkView'),
    url(r'^linkEditView/$', api_views.LinkEditView.as_view(), name='linkEditView'),
    url(r'^adEditView/$', api_views.AdEditView.as_view(), name='adEditView'),
    url(r'^uploadFile/$', api_views.UploadFile.as_view(), name='uploadFile'),
    url(r'^chapterIsPay/$', api_views.ChapterIsPay.as_view(), name='chapterIsPay'),
    url(r'^uploadFile/$', api_views.UploadFile.as_view(), name='uploadFile'),
    url(r'^novelEdit/$', api_views.NovelEdit.as_view(), name='novelEdit'),
    url(r'^navigationEdit_display/$', api_views.Navigation.as_view(), name='navigationEdit_display'),
    url(r'^novelShow/$', api_views.NovelShow.as_view(), name='novelShow'),
    url(r'^adListView/$', views.AdView.as_view(), name='adListView'),
    url(r'^saveMode/$', views.SaveMode.as_view(), name='saveMode'),
    url(r'^normalAccess/$', views.NormalAccess.as_view(), name='normalAccess'),
    url(r'^freezeAccess/$', views.FreezeAccess.as_view(), name='freezeAccess'),
    url(r'^forbidAccess/$', views.ForbidAccess.as_view(), name='forbidAccess'),
]
