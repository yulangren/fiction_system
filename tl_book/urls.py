"""tl_book URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from system import urls as admin_url
from api import urls as api_url
from web_api import urls as web_api
from web import urls as web_urls
from django.views.generic import RedirectView
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(web_urls, namespace='web_system', app_name='web_system')),
    url(r'^admin/', include(admin_url, namespace='admin_system', app_name='admin_system')),
    url(r'^api/', include(api_url, namespace='api_system', app_name='api_system')),
    url(r'^web_api/', include(web_api, namespace='web_api', app_name='web_api')),
    url(r'^favicon\.ico$', RedirectView.as_view(url=r'static/img/favicon.ico')),
]
