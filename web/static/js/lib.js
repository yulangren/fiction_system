// 消息通知
function notify(obj, title, msg, type) {
    obj.$notify({
        title: title,
        message: msg,
        type: type
    });
}

// form 提交
function ajax(obj, method = 'POST', data_json = {}, url = '', is_show = true) {
    result = {};
    $.ajax({
        type: method,
        url: url,
        data: data_json,
        dataType: 'json',
        async: false,
        timeout: 10000,
        headers: {'X-CSRFToken': this.getCookie('csrftoken')},
        success: function (response) {
            result = response;
            if (response.code === 0) {
            } else if (response.code === 4) {
                if (is_show) {
                    notify(obj, response.msg, response.error, 'warning');
                }
            } else {
                if (is_show) {
                    notify(obj, response.msg, response.error, 'error');
                }
            }
        }, error: function (err) {
            notify(obj, '服务器处理异常', '请联系管理员处理！', 'warning');
        },
        complete: function (XMLHttpRequest, textStatus) {
            if (textStatus === 'timeout') {
                notify(obj, '请求超时', '请求超时或服务器处理时间过长', 'warning');
            }
        }

    });
    return result
}

// 获取Cookie
function getCookie(name) {
    var value = '; ' + document.cookie;
    var parts = value.split('; ' + name + '=');
    if (parts.length === 2) return parts.pop().split(';').shift()
}

// 日期时间对象转字符串
function dataTime_to_string(val) {
    return val.getFullYear() + "-" + (val.getMonth() + 1) + "-" + val.getDate() + " " + val.getHours() + ":" + val.getMinutes() + ":" + val.getSeconds() + "." + val.getMilliseconds();
}

$(function () {
    $('#app').find('a').on('click', function (e) {
        uid = $(this).attr('data-id'); // 小说ID
        sort_uid = $(this).attr('sort_id');
        chapter_id = $(this).attr('data-chapter_id'); // 章节ID
        _location = $(this).attr('location'); // 章节ID
        taget = $(this).attr('target'); //_self todo 判断跳转页面方式
        if (uid) {
            console.log('书:', uid);
            if (taget === '_self') {
                self.location = '/book/' + uid + '.jsp'
            } else {
                window.open('/book/' + uid + '.jsp');
            }
        } else if (sort_uid) {
            // console.log('分类ID：', sort_uid);
            if (sort_uid.length === 36 && !(sort_uid.slice(0, 7) === 'http://' || sort_uid.slice(0, 7) === 'https:/')) {
                url = "/sort/" + sort_uid + ".jsp";
                if (taget === '_self') {
                    self.location = url
                } else {
                    window.location.href = url
                }

            } else if (sort_uid.length && (sort_uid.slice(0, 7) === 'http://' || sort_uid.slice(0, 7) === 'https:/')) {
                if (taget === '_self') {
                    self.location = sort_uid
                } else {
                    window.open(sort_uid)
                }
            } else {
                if (taget === '_self') {
                    self.location = sort_uid;
                } else {
                    location.href = sort_uid;
                }
            }

        } else if (chapter_id) {
            url = "/detail/" + chapter_id + ".jsp";
            if (taget === '_self') {
                self.location = url
            } else {
                window.open(url);
            }
            // = url;
            console.log('章节：', chapter_id)
        } else if (_location !== undefined) {
            self.location = _location;
        }
    })
});

function handleNavSelect(key, keyPath) {
    if (key.length === 36 && !(key.slice(0, 7) === 'http://' || key.slice(0, 7) === 'https:/')) {
        url = "/sort/" + key + ".jsp";
        window.location.href = url
    } else if (key.length && (key.slice(0, 7) === 'http://' || key.slice(0, 7) === 'https:/')) {
        window.open(key);
    } else if (key === '/') {
        window.location.href = window.location.protocol + "//" + window.location.host
    }else {
        window.location.href =key
    }
}