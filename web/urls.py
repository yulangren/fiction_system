from django.conf.urls import url
from web import views

urlpatterns = [
    url(r'^$', views.IndexPage.as_view(), name='indexPage'),
    url(r'^sort/(.+).jsp$', views.SortList.as_view(), name='sortList'),
    url(r'^detail/(.+).jsp$', views.Detail.as_view(), name='detail'),
    url(r'^book/(.+).jsp$', views.Book.as_view(), name='book'),
    url(r'^ranking_list.jsp$', views.OtherList.as_view(), name='rankingList'),  # 排行榜
    url(r'^over.jsp$', views.OtherList.as_view(), name='overList'),  # 完本
    url(r'^serialize.jsp$', views.OtherList.as_view(), name='serializeList'),  # 连载
]
