from django.shortcuts import render
# Create your views here.
from django.http import Http404, HttpResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from decorator import normal_auth, freeze_auth, forbid_auth
from public import decompress
from system.models import ChapterModel
from web_api.lib import novelSerialize


@method_decorator(forbid_auth, name='dispatch')
@method_decorator(freeze_auth, name='dispatch')
@method_decorator(normal_auth, name='dispatch')
class IndexPage(View):
    def get(self, request):
        return render(request, 'index.html')


@method_decorator(forbid_auth, name='dispatch')
@method_decorator(freeze_auth, name='dispatch')
@method_decorator(normal_auth, name='dispatch')
class SortList(View):
    def get(self, request, uid):
        return render(request, 'sort.html', {'uid': uid})


@method_decorator(forbid_auth, name='dispatch')
@method_decorator(freeze_auth, name='dispatch')
@method_decorator(normal_auth, name='dispatch')
class Book(View):
    def get(self, request, bid):  # 验证bid 抛404
        return render(request, 'book.html', {'book_id': bid})


@method_decorator(forbid_auth, name='dispatch')
@method_decorator(freeze_auth, name='dispatch')
@method_decorator(normal_auth, name='dispatch')
class Detail(View):
    def get(self, request, cid):  # 验证cid 抛404
        try:
            chapter = ChapterModel.objects.filter(is_delete=False, uuid=cid).first()
            if chapter is None: raise Http404()
        except:
            return Http404()
        data = dict(chapter)
        data['novel'] = chapter.novel.novel_name
        data['novel_id'] = chapter.novel.uuid
        data['content'] = decompress(data['content_path'], data['content_id'])
        data['novel_author'] = chapter.novel.novel_author
        data['novel_name'] = chapter.novel.novel_name
        data['novel_description'] = chapter.novel.novel_description
        data['novel_key'] = chapter.novel.novel_key
        chapter_list = novelSerialize(chapter.novel)
        chapterList = chapter_list['chapter']
        first, fast = {}, {}  # 上一个，下一个
        current_chapter = dict(chapter)
        current_chapter['uuid'] = chapter.uuid
        current_chapter['novel'] = chapter.novel.novel_name
        if current_chapter.get('is_pay'):
            return HttpResponse("当前章节为付费内容！")  # todo 章节付费可见  ==》  更换模板
        if chapterList.index(current_chapter) != 0 and len(chapterList) - 1 > chapterList.index(current_chapter):  # 中间
            first, fast = chapterList[chapterList.index(current_chapter) + 1], chapterList[chapterList.index(current_chapter) - 1]
        elif chapterList.index(current_chapter) == 0 and len(chapterList) - 1 > chapterList.index(current_chapter):  # 最后一页
            first = chapterList[chapterList.index(current_chapter) + 1]
        elif chapterList.index(current_chapter) == len(chapterList) - 1:  # 第一页
            fast = chapterList[chapterList.index(current_chapter) - 1]
        data['first'] = first
        data['fast'] = fast
        return render(request, 'detail.html', {'chapter': data})


@method_decorator(forbid_auth, name='dispatch')
@method_decorator(freeze_auth, name='dispatch')
@method_decorator(normal_auth, name='dispatch')
class OtherList(View):
    def get(self, request):
        return render(request, 'other_list.html', {'path': request.path})
