from django.conf.urls import url
from web_api import views

urlpatterns = [
    url(r'^navigation/$', views.Navigation.as_view(), name='navigation'),
    url(r'^getNovel/$', views.GetNovel.as_view(), name='getNovel'),
    url(r'^navigationNovel/$', views.NavigationNovel.as_view(), name='navigationNovel'),
    url(r'^links/$', views.Links.as_view(), name='links'),
    url(r'^sortNovel/$', views.SortNovel.as_view(), name='sortNovel'),
    url(r'^book/$', views.BookData.as_view(), name='book'),
    url(r'^getOther/$', views.GetOther.as_view(), name='getOther'),
    url(r'^search/$', views.Search.as_view(), name='search'),
]
